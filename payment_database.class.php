<?php
/*
 * Interface to the Authorize.net Auto Recurring Billing system
 * 
 * All database interaction is done through the members of this class.
 * 
 * Started 4/9/12 by James Hickman <jhickman@arcdigitalservices.com>
 */
class PaymentDatabase {
	/*
	 * Constructor:
	 * Params:
	 * $db          MySQLi connection object
	 */
	function PaymentDatabase ($db, $acctid, $acctn) {
		if (get_class($db) != 'mysqli') {
			throw new Exception ("Database handel is not a MySQLi object!");
		}
		$this->dbc = $db;
		$this->acctid = $acctid;
		//print("\nAccount ID: " . $this->acctid . "\n");
		$this->acctName = $acctn;
	}
	
	/*
	 * Save the details of the ARB setup to the database for auditing.
	 */
	function record_arb_created($start_date, $billing_dom, $cycles_set, $arb_sub_id) {
		$q = "INSERT INTO authrizenet_arb (acct_id, acct_name, billing_start, billing_dom, cycles_set, arb_sub_id) VALUES (?, ?, ?, ?, ?, ?)";
		$stmt = $this->dbc->prepare($q);
		$stmt->bind_param("issiis", $this->acctid, $this->acctName, $start_date, $billing_dom, $cycles_set, $arb_sub_id);
		$stmt->execute();
	}
	
	function record_arb_reprice_update($price) {
		// Get the ID for this authrizenet_arb for this ARB
		$q = "SELECT id FROM authrizenet_arb WHERE acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		$recid = $row['id'];
		// Innsert a new record into authrizenet_arb_updates
		$q = "INSERT INTO authrizenet_arb_updates (authrizenet_arb_id, amount) VALUES (" . $this->dbc->real_escape_string($recid) . ", " . $this->dbc->real_escape_string($price) . ")";
		$this->dbc->query($q);
	}
	
	/*
	 * Get all the Package features' prices for the current account
	 * and return the sum.
	 */
	function sum_cost_of_packages() {
			//echo("\nAccount ID: $this->acctid\n");
		// Need the tables that define the Packages
		$q = "SELECT price FROM package_price WHERE acccount = " . $this->dbc->real_escape_string($this->acctid);
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		return $row['price'];
	}
	
	/*
	 * Return day of month that the charge is put through.
	 */
	function get_arb_billing_dom() {
		$q = "SELECT billing_dom FROM authrizenet_arb WHERE acct_id = " . $this->dbc->real_escape_string($this->acctid);
		//print("\n$q\n");
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		return $row['billing_dom'];
	}
	
	/*
	 * Get ARB account ID.
	 * 
	 * Return the Authorize.net ARB record for the current account.
	 */
	function get_arb_id() {
		$q = "SELECT arb_sub_id FROM authrizenet_arb WHERE acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		return $row['arb_sub_id'];
	}
	
	/*
	 * Get the dirty flag.
	 */
	function get_prorate_dirty() {
		$q = "SELECT prorate_dirty FROM authrizenet_arb WHERE acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		return $row['prorate_dirty'];
	}
	
	function set_prorate_dirty() {
		$q = "UPDATE authrizenet_arb SET prorate_dirty = true WHERE acct_id = ?";
		$stmt = $this->dbc->prepare($q);
		$stmt->bind_param("i", $this->acctid);
		$stmt->execute();
	}
	
	/*
	 * Get all updates between two dates
	 */
	function get_updates_between($ts_from, $ts_to) {
		$q = "SELECT p.amount, p.updated FROM authrizenet_arb AS a, authrizenet_arb_updates AS p WHERE p.authrizenet_arb_id = a.id ";
		$q .= " AND a.acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$q .= " AND p.updated > '" . $this->dbc->real_escape_string($ts_from) . "'";
		$q .= " AND p.updated < '" . $this->dbc->real_escape_string($ts_to) . "'";
		$q .= " ORDER BY p.updated";
		//print("\n$q\n");
		$result = $this->dbc->query($q);
		$ret = array();
		while($row = $result->fetch_assoc()) {
			$ret[] = $row;
		}
		print_r($ret);
		return $ret;
	}
	
	/*
	 * Get the last price from before the passed MySQL format TIMESTAMP
	 */
	function get_update_before($myts_start) {
		$q = "SELECT p.amount AS pr FROM authrizenet_arb AS a, authrizenet_arb_updates AS p WHERE p.authrizenet_arb_id = a.id";
		$q .= " AND a.acct_id = '" . $this->dbc->real_escape_string($this->acctid) . "'";
		$q .= " AND p.updated < '" . $this->dbc->real_escape_string($myts_start) . "'";
		$q .= " ORDER BY p.updated DESC LIMIT 1";
		//print("\n$q\n");
		$result = $this->dbc->query($q);
		if ($this->dbc->field_count > 0) {
			$row = $result->fetch_assoc();
			return $row["pr"];
		}
		else {
			return 0;
		}
	}
	
	/*
	 * Is there an ARB set up for this account?
	 */
	function is_an_arb() {
		$q = "SELECT count(*) AS c FROM authrizenet_arb WHERE acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		if ($row['c'] == 0)
			return False;
		else
			return True;
	}
	
	/*
	 * The last attempt to update an ARB failed.
	 */
	function arb_update_fault() {
		// Get the last update record for this account and set the fault flag.
		$q = "SELECT u.id AS id FROM authrizenet_arb AS a, authrizenet_arb_updates AS u WHERE u.authrizenet_arb_id = a.id ";
		$q .= " AND a.acct_id = " . $this->dbc->real_escape_string($this->acctid);
		$q .= " ORDER BY u.updated DESC LIMIT 1";
		$result = $this->dbc->query($q);
		$row = $result->fetch_assoc();
		$id = $row['id'];
		
		$q = "UPDATE authrizenet_arb_updates SET fault=1 WHERE id = $id";
		$result = $this->dbc->query($q);
	}
	
	/*
	 * Dump some data structure to the generic error log table
	 */
	function generic_log($code, $o) {
		$q = "INSERT INTO high_level_logging (system, account_id, code, dump) VALUES (";
		$q .= "'BILLING',";
		$q .= "'" . $this->dbc->real_escape_string($this->acctid) . "',";
		$q .= "'" . $this->dbc->real_escape_string($code) . "',";
		$q .= "'" . $this->dbc->real_escape_string(var_export($o, True)) . "'";
		$q .= ")";
		$this->dbc->query($q);
	}
}
?>
