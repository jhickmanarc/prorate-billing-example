<?php
/*
 * Interface to the Authorize.net Auto Recurring Billing system
 * 
 * Computation of charges including prorating on a service change go here.
 * 
 * Started 4/9/12 by James Hickman <jhickman@arcdigitalservices.com>
 */
class AccountPriceComputations {
	function AccountPriceComputations($dbObj, $acct, $t) {
		$this->dbo = $dbObj;		// Instance of PaymentDatabase
		$this->accountod = $acct;	// Account ID index
		$this->now = $t;			// Current UNIX timestamp
	}
	
	/*
	 * Get the Package features for the current Account and
	 * tabulate the monthly fee for the specified selection.
	 */
	function price_for_current_package() {
		return $this->dbo->sum_cost_of_packages();
	}
	
	/*
	 * Conduct Prorate computation.
	 * 
	 * Returns the prorate adjusted charge for the next billing cycle.
	 */
	function prorate_service_change() {
		// Get the price for the current Package selection.
		$rate = $this->price_for_current_package();
		// Save the new price record in the authrizenet_arb_updates table.
		$this->dbo->record_arb_reprice_update($rate);
		
		// Get the Billing day of month
		$bdom = $this->dbo->get_arb_billing_dom();
		//print("\nBilling DOM: $bdom\n");
		
		// Calculate the last billing date and the next billing date
		$nowdom = date("j", $this->now);
		//print("\nToday DOM: $nowdom\n");
		$nowmonth = date("n", $this->now);
		//print("\nNow month: $nowmonth\n");
		$nowyear = date("Y", $this->now);
		//print("\nNow year: $nowyear\n");
		// If current DOM >= billing DOM then last billing was in current month and next billing next month.
		// Else last billing was previous month and next billing in current month.
		if ($nowdom >= $bdom) {
			$bill_last_day = $bdom;
			$bill_last_month = $nowmonth;
			$bill_last_year = $nowyear;
			
			$bill_next_day = $bdom;
			$bill_next_month = $nowmonth + 1;
			$bill_next_year = $nowyear;
			
			// If running over to the next year.
			if ($bill_next_month > 12) {
				$bill_next_month = 1;
				$bill_next_year += 1;
			}
		}
		else {
			$bill_last_day = $bdom;
			$bill_last_month = $nowmonth - 1;
			$bill_last_year = $nowyear;
			// If falling back to last year.
			if ($bill_last_month < 1) {
				$bill_last_month = 12;
				$bill_last_year -= 1;
			}
			
			$bill_next_day = $bdom;
			$bill_next_month = $nowmonth;
			$bill_next_year = $nowyear;
		}
		
		//print("\nStart date: $bill_last_year-$bill_last_month-$bill_last_day\nTo date: $bill_next_year-$bill_next_month-$bill_next_day\n");
		
		// Compute the number of days between the dates.
		$last_ts = strtotime("$bill_last_year-$bill_last_month-$bill_last_day");
		$next_ts = strtotime("$bill_next_year-$bill_next_month-$bill_next_day");
		$current_bill_period_length = floor(abs($next_ts - $last_ts)/(60*60*24));
		
		//print("\nCurrent cycle length: $current_bill_period_length\n");
		
		// Get any updates that fall between the two dates.
		$myts_from = $bill_last_year . '-' . str_pad((int) $bill_last_month,2,"0",STR_PAD_LEFT) . '-' . str_pad((int) $bill_last_day,2,"0",STR_PAD_LEFT) . " 00:00:00";
		$myts_to = $bill_next_year . '-' . str_pad((int) $bill_next_month,2,"0",STR_PAD_LEFT) . '-' . str_pad((int) $bill_next_day,2,"0",STR_PAD_LEFT) . " 00:00:00";
		$prorate_records = $this->dbo->get_updates_between($myts_from, $myts_to);
		
		// Get the rate for the last update before this billing period.
		$past_rate = $this->dbo->get_update_before($myts_from);
		if ($past_rate > 0) {
			$lastrate = $past_rate / $current_bill_period_length;
		}
		else {
			$lastrate = 0;
		}
		//print("\nOld rate per day: $lastrate\n");
				
		// Construct an array of number of days on each rate and the rate for that period.
		/*
		 * Fields in $prorate:
		 *     price      - Price for each day, month rate / $current_bill_period_length.
		 *     days       - Number of days at this rate.
		 */
		$prorates = array();
		// Start from the end date of the current cycle, make that the place holder.
		$ts_place = $next_ts;
		
		// Loop backward through the results, latest change to earliest
		for ($i = count($prorate_records); $i > 0; $i--) {
			//print("\nLoop index: $i, timestamp: $ts_place\n");
			$tmp = array();
			// Get the tmie of change from the present row, subtract from the time place holder for the interval.
			$ts_row = strtotime($prorate_records[$i - 1]['updated']);
			$tmp['days'] = floor(($ts_place - $ts_row)/(60*60*24));
			// Compute the daily rate for the row.
			$tmp['price'] = $prorate_records[$i - 1]['amount'] / $current_bill_period_length;
			
			// Set the place holder to the row's time.
			$ts_place = $ts_row;
			//print("\nTimestamp after loop: $ts_place\n");
			
			$prorates[] = $tmp;
		}
		if ($lastrate > 0) {
			$tmp = array();
			// After the loop subtract the start from the place holder for the run of days before change.
			// Set the rate to what it was before changes.
			$tmp['price'] = $lastrate;
			$tmp['days'] = ($ts_place - $last_ts)/(60*60*24);
			$prorates[] = $tmp;
		}
		
		//print_r($prorates);
		
		// Loop over the array and calculate the bext billing amount.
		$adjusted = 0;
		foreach ($prorates as $prorate) {
			$adjusted += $prorate['days']*$prorate['price'];
		}
		
		// Set the Dirty flag
		$this->dbo->set_prorate_dirty();
		
		// Return the pro-rate adjusted amount to charge on the next billing.
		// Rounded to cents.
		return round($adjusted, 2);
	}
}
?>
