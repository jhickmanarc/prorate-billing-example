-- Maintain a record of Automated Billing Requests.
CREATE TABLE authrizenet_arb (
    id                          INTEGER AUTO_INCREMENT,
    created                     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    acct_id                     INTEGER,                        -- Database ID of the dashboard Account.
    acct_name                   VARCHAR(255),                   -- Name of the Dashboard.
    billing_start               DATE,                           -- The date tyhe first bill is charged.
    billing_dom                 INTEGER,                        -- Day of month that the charge is made.
    cycles_set                  INTEGER,                        -- How many cycles where set when ARB created.
    active                      BOOL NOT NULL DEFAULT 1,
    prorate_dirty               BOOL NOT NULL DEFAULT 0,        -- Flag set if the billing amount has been adjusted for a prorating for the current cycle.
                                                                -- On the first day of the next cycle reset the charges to the current set of Packages price.
    arb_sub_id                  VARCHAR(255),                   -- ARB reference ID returned from Authorize.net
    PRIMARY KEY(id)
);

-- Record updates to the amount billed monthly.
CREATE TABLE authrizenet_arb_updates (
    id                          INTEGER AUTO_INCREMENT,
    authrizenet_arb_id          INTEGER,                                        -- Foreign Key to authrizenet_arb.
    updated                     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,   -- Current date and time.
    amount                      FLOAT,                                          -- The amount set to charge.
    fault                       BOOL NOT NULL DEFAULT 0,                        -- Set when there is a fault setting the update with Authorize.net
	PRIMARY KEY(id)
);

-- Test Harness tables.
CREATE TABLE package_price (
    acccount                    INTEGER,
    price                       FLOAT
);


-- Generic high level Incident log table.
CREATE TABLE high_level_logging (
    id                          INTEGER AUTO_INCREMENT,
    created                     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    system                      VARCHAR(255),
    account_id                  INTEGER NOT NULL,
    code                        VARCHAR(255),
    dump                        BLOB,
    PRIMARY KEY(id)
);
