<?php
/*
 * Interface to the Authorize.net Auto Recurring Billing system
 * 
 * Core interface class to communicate with Authorize.net and provide
 * back end and validation to the user forms.
 * 
 * Started 4/9/12 by James Hickman <jhickman@arcdigitalservices.com>
 */
 
include_once(dirname(__FILE__) . "/anet_php_sdk/AuthorizeNet.php");
include_once(dirname(__FILE__) . "/payment_database.class.php");
include_once(dirname(__FILE__) . "/arb_price_calculations.class.php");
include_once(dirname(__FILE__) . "/authorizenet_settings.inc.php");

class ARBpayment {
	/*
	 * Constructor:
	 * Params:
	 *     $db        MySQLi database connection object.
	 *     $uid       User Identifier
	 *     $acctid    Account identifier
	 *     $t         Current UNIX timestamp, set outside for easy testing of prorates.
	 */
	function ARBpayment($db, $acctid, $dashn, $t) {
		if (get_class($db) != 'mysqli') {
			throw new Exception ("Database handel is not a MySQLi object!");
		}
		$this->db_o = new PaymentDatabase($db, $acctid, $dashn);
		//print("\nAccount ID: \n");
		$this->price_o = new AccountPriceComputations($this->db_o, $acctid, $t);
		$this->accountID = $acctid;
		$this->dashName = $dashn;
		$this->now = $t;
		$this->config = array(
			'init_length' => 12,
			'init_totalOccurrences' => 12,
			'init_trialOccurrences' => 0,
			'init_trialAmount' => 0
		);
	}
	
	/*
	 * Generate and process the form.
	 * 
	 * POST Fields:
	 *     first           Customer First name
	 *     last            Last name
	 *     mi              Middle initial
	 *     card            CC number
	 *     expire_y        Experation date year
	 *     expire_m        Experation month
	 *     varification    Card verification code
	 * 
	 * Returns an object with fields:
	 *     messages    Tupple of validation messages for each field.
	 *     valid       boolean
	 */
	function formpost($post) {
		$ret = array (
			'valid' => True,
			'messages' => array (
				'first' => '',
				'last' => '',
				'mi' => '',
				'card' => '',
				'expire_y' => '',
				'expire_m' => '',
				'verification' => '',
				'message' => ''
			)
		);
		// Basic Validations.
		if (strlen($post['first']) < 1) {
			$ret['valid'] = False;
			$ret['messages']['first'] = "First Name required.";
		}
		
		if (strlen($post['last']) < 1) {
			$ret['valid'] = False;
			$ret['messages']['last'] = "Last Name required.";
		}
		
		if (strlen($post['mi']) < 1 || strlen($post['mi']) > 1) {
			$ret['valid'] = False;
			$ret['messages']['mi'] = "Middle Initial required.";
		}

		if (!is_numeric($post['expire_y']) || strlen($post['expire_y']) != 4) {
			$ret['valid'] = False;
			$ret['messages']['expire_y'] = "Year invalid.";
		}
		
		if (!is_numeric($post['expire_m']) || strlen($post['expire_m']) != 2) {
			$ret['valid'] = False;
			$ret['messages']['expire_m'] = "Month Invalid";
		}
		
		if (!is_numeric($post['verification'])) {
			$ret['valid'] = False;
			$ret['messages']['verification'] = "Invalid code.";
		}
		
		// If basic validators passed Attempt to create subscription.
		if ($ret['valid']) {
			$result = $this->create_authorizenet_arb($post);
			
			if ($result['success'] != True) {
				// Something went wrong!
				$ret['valid'] = False;
				$code = $result['anetresp']->xpath_xml->messages->message[0]->code;
				//print("\nStatus code: $code\n");
				$text = $result['anetresp']->xpath_xml->messages->message[0]->text;
				// Log to the Apache logs
				switch ($code) {
					// Define validation messages based on the return error from Authorize.net.
					case "E00013":
						$ret['messages']['message'] = "The credit card is not valid as of the start date of the subscription.";
						break;
					case "E00016":
						$ret['messages']['card'] = "The credit card number is invalid.";
						break;
					case "E00027":
						$ret['messages']['card'] = "The credit card number is invalid.";
						break;
					default:
						$ret['messages']['message'] = "Unknown error from Payment Processer! :$code";
				}
			}
		}
		return $ret;
	}
	
	/*
	 * Attempt to set up an ARB on Authorize.net.
	 */
	function create_authorizenet_arb ($formdata) {
		$refId = $this->accountID . $this->now;
		$name = $this->dashName . " (" . $formdata['first'] . ' ' . $formdata['last'] . ")";
		$startDate = $this->calculate_billing_start_date();
		$dom = $this->calculate_billing_day();
		$amount = $this->price_o->price_for_current_package();
		
		$subscription = new AuthorizeNet_Subscription;
        $subscription->name = $name;
        $subscription->intervalLength = "1";
        $subscription->intervalUnit = "months";
        $subscription->startDate = $startDate;
        $subscription->totalOccurrences = $this->config['init_totalOccurrences'];
        $subscription->amount = $amount;
        $subscription->creditCardCardNumber = $formdata['card'];
        $subscription->creditCardExpirationDate = $formdata['expire_y'] . "-" . $formdata['expire_m'];
        $subscription->creditCardCardCode = $formdata['verification'];
        $subscription->billToFirstName = $formdata['first'];
        $subscription->billToLastName = $formdata['last'];
        
        //print_r($subscription);
        
        $request = new AuthorizeNetARB;
        $response = $request->createSubscription($subscription);
				// ******************************* Debugging ***************************
				//print_r($response);
        
        $res = array('success' => True, 'anetresp' => $response);
        
        if ($response->isOk()) {
			// Success!!
			$this->db_o->record_arb_created($startDate, $dom, $this->config['init_totalOccurrences'], $response->getSubscriptionId());
			$this->db_o->record_arb_reprice_update($amount);
		}
		else {
			// Something went wrong!
			$res['success'] = False;
		}
		return $res;
	}
	
	/*
	 * Billing cycle starts the day of sign-up unless 
	 * that day > 28 then the start is the first day of the 
	 * following month.
	 */
	function calculate_billing_start_date() {
		$month = date('n', $this->now);
		$year = date('Y', $this->now);
		$dom = date("j", $this->now);
		if ($dom > 28) {
			$dom = 1;
			$month += 1;
			if ($month > 12) {
				$month = 1;
				$year += 1;
			}
		}
		return $year.'-'.str_pad((int) $month,2,"0",STR_PAD_LEFT).'-'.str_pad((int) $dom,2,"0",STR_PAD_LEFT);
	}
	
	/*
	 * Compute the day of the month that the charge is made.
	 */
	function calculate_billing_day() {
		// Get the current DOM
		$dom = date("j", $this->now);
		if ($dom > 28) {
			$dom = 1;
		}
		return $dom;
	}
	
	/*
	 * Update the existing ARB for the current dashboard Account
	 * specified in the Constructor.
	 */
	function update_authorizenet_arb($amount) {
		// Fetch the ARB Account ID
		$subscription_id = $this->db_o->get_arb_id();
		
		// Build the objects to upadte the amount on the account.
		$update_request = new AuthorizeNetARB;
		$updated_subscription_info = new AuthorizeNet_Subscription;
		$updated_subscription_info->amount = $amount;
		$update_response = $update_request->updateSubscription($subscription_id, $updated_subscription_info);
		if ($update_response->isOk()) {
			return True;
		}
		else {
			// Log the error updating.
			//print_r($update_response);
			//error_log("Authorize.net: Error updating ARB $subscription_id. " . var_export($update_response, True));
			$this->db_o->arb_update_fault();
			$this->db_o->generic_log("Failed to update ARB", $update_response);
			return False;
		}
	}
	
	/*
	 * Call if the Packages have been altered and charges have to be prorated.
	 */
	function do_prorate_for_new_package() {
		// Get the prorated price from the price calculation object.
		$price = $this->price_o->prorate_service_change();
		
		//print("\nEffective pro-rated price for next billing: $price\n");
		
		// Update the price with Authorize.net
		$this->update_authorizenet_arb($price);
	}
}
?>
